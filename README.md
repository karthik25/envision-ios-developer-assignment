# Envision - iOS Developer Assignment

This is the take home assignment for an iOS Developer at Envsion. This project should be forked and karthik@letsenvision.com should be added to it at the time of submission. 

## The Project: Envision Highlighter

Envision Highlighter is an accessible book Highlighter that can highlight text from books and display them in an accessible format for blind and visually impaired people. The Highlighter app contains two screens. The entry point is a screen that contains a camera feed to highlight text. The 2nd screen is there to display the highlighted text -- this screen should VoiceOver accessible. There's also a database system that allows users to keep adding new highlights under an existing book.

## Design

You'll find the design here along with notes on each screen: https://www.figma.com/file/mc8Ahvua2SU1sSpFLdchOp/%F0%9F%94%B5-Assignments?node-id=0%3A1


## Scoring system for Envision Highlighter


| Task | Score |
| ------ | ------ |
| Stick to the Figma design for all screens | 2 |
| Build a performant AVFoundation setup that works across device types | 3 |
| Make the app fully compatible with Dynamic Type | 4 |
| Make use of SwiftUI | 5 |
| Make the app fully VoiceOver compatible with proper acessibilty labels | 6 |

